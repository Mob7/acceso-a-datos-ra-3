package Base;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by sergio on 06/02/2016.
 */
@Entity
@Table(name="discograficas")
public class Discografica {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Column(name="nombre_registrado")
    private String nombre;

    @Column(name="dinero_facturado")
    private float dineroFacturado;

    @Column(name="artistas_contratados")
    private int artistasContratados;

    @Column(name="fecha_registro")
    private Date fechaRegistro;

    @OneToMany(mappedBy = "discografica")
    private List<Disco> disco;

    public Discografica(){
        disco = new ArrayList<Disco>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getDineroFacturado() {
        return dineroFacturado;
    }

    public void setDineroFacturado(float dineroFacturado) {
        this.dineroFacturado = dineroFacturado;
    }

    public int getArtistasContratados() {
        return artistasContratados;
    }

    public void setArtistasContratados(int artistasContratados) {
        this.artistasContratados = artistasContratados;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public List<Disco> getDisco() {
        return disco;
    }

    public void setDisco(List<Disco> disco) {
        this.disco = disco;
    }

    public String toString(){
        return nombre;
    }
}
