package Base;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by sergio on 06/02/2016.
 */
@Entity
@Table(name="discos")
public class Disco {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Column(name="titulo")
    private String titulo;

    @Column(name="precio_compra")
    private float precioCompra;

    @Column(name="copias_vendidas")
    private int copiasVendidas;

    @Column(name="fecha_salida")
    private Date fechaSalida;

    @ManyToOne
    @JoinColumn(name="id_artista")
    private Artista artista;
    @ManyToOne
    @JoinColumn(name="id_discografica")
    private Discografica discografica;
    @ManyToMany(mappedBy = "discos")
    private List<Categoria> categorias;

    public Disco(){
        categorias = new ArrayList<Categoria>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public float getPrecioCompra() {
        return precioCompra;
    }

    public void setPrecioCompra(float precioCompra) {
        this.precioCompra = precioCompra;
    }

    public int getCopiasVendidas() {
        return copiasVendidas;
    }

    public void setCopiasVendidas(int copiasVendidas) {
        this.copiasVendidas = copiasVendidas;
    }

    public Date getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(Date fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public Artista getArtista() {
        return artista;
    }

    public void setArtista(Artista artista) {
        this.artista = artista;
    }

    public Discografica getDiscografica() {
        return discografica;
    }

    public void setDiscografica(Discografica discografica) {
        this.discografica = discografica;
    }

    public List<Categoria> getCategorias() {
        return categorias;
    }

    public String toString() {
        return titulo+" - "+this.artista.toString();
    }
}
