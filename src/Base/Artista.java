package Base;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by sergio on 06/02/2016.
 */
@Entity
@Table(name="artistas")
public class Artista {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Column(name="nombre")
    private String nombre;

    @Column(name="telefono")
    private int telefono;

    @Column(name="precio_actuacion")
    private float precioActuacion;

    @Column(name="fecha_nacimiento")
    private Date fechaNacimiento;

    @OneToMany(mappedBy="artista")
    private List<Cancion> canciones;
    @OneToMany(mappedBy="artista")
    private List<Disco> discos;
    @OneToMany(mappedBy="artista")
    private List<ArtistasConciertos> artistaConcierto;

    public Artista(){
        canciones = new ArrayList<Cancion>();
        discos = new ArrayList<Disco>();
        artistaConcierto = new ArrayList<ArtistasConciertos>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public float getPrecioActuacion() {
        return precioActuacion;
    }

    public void setPrecioActuacion(float precioActuacion) {
        this.precioActuacion = precioActuacion;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public List<Cancion> getCanciones() {
        return canciones;
    }

    public void setCanciones(List<Cancion> canciones) {
        this.canciones = canciones;
    }

    public List<Disco> getDiscos() {
        return discos;
    }

    public void setDiscos(List<Disco> discos) {
        this.discos = discos;
    }

    @Override
    public String toString(){
        return nombre;
    }
}
