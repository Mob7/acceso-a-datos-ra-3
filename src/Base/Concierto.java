package Base;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by sergio on 06/02/2016.
 */
@Entity
@Table(name="conciertos")
public class Concierto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Column(name="nombre")
    private String nombre;

    @Column(name="recaudacion")
    private float recaudacion;

    @Column(name="ciudad")
    private String ciudad;

    @Column(name="fecha")
    private Date fecha;

    @OneToMany(mappedBy="concierto")
    private List<ArtistasConciertos> artistaConcierto;

    public Concierto(){
        artistaConcierto = new ArrayList<ArtistasConciertos>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getRecaudacion() {
        return recaudacion;
    }

    public void setRecaudacion(float recaudacion) {
        this.recaudacion = recaudacion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public List<ArtistasConciertos> getArtistaConcierto() {
        return artistaConcierto;
    }

    public void setArtistaConcierto(List<ArtistasConciertos> artistaConcierto) {
        this.artistaConcierto = artistaConcierto;
    }

    public String toString(){
        return nombre;
    }
}
