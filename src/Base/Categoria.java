package Base;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sergio on 09/02/2016.
 */
@Entity
@Table(name="categorias")
public class Categoria {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name="nombre")
    private String nombre;
    @Column(name="descripcion")
    private String descripcion;

    @ManyToMany(cascade = CascadeType.DETACH)
    @JoinTable(name="discos_categorias",
            joinColumns={@JoinColumn(name="id_categoria")},
            inverseJoinColumns={@JoinColumn(name="id_disco")})
    List<Disco> discos;

    public Categoria(){
        discos = new ArrayList<Disco>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<Disco> getDiscos() {
        return discos;
    }

    public void setDiscos(List<Disco> discos) {
        this.discos = discos;
    }

    public String toString() {
    return nombre;
    }
}
