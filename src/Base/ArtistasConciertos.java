package Base;

import javax.persistence.*;

/**
 * Created by sergio on 06/02/2016.
 */
@Entity
@Table(name = "artistas_conciertos")
public class ArtistasConciertos {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @ManyToOne
    @JoinColumn(name="id_artista")
    private Artista artista;
    @ManyToOne
    @JoinColumn(name="id_concierto")
    private Concierto concierto;

    @Column(name="cache")
    private float cache;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Artista getArtista() {
        return artista;
    }

    public void setArtista(Artista artista) {
        this.artista = artista;
    }

    public Concierto getConcierto() {
        return concierto;
    }

    public void setConcierto(Concierto concierto) {
        this.concierto = concierto;
    }

    public float getCache() {
        return cache;
    }

    public void setCache(float cache) {
        this.cache = cache;
    }
}
