package Base;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by sergio on 06/02/2016.
 */

@Entity
@Table(name="canciones")
public class Cancion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Column(name="titulo")
    private String titulo;

    @Column(name="precio_itunes")
    private float precioItunes;

    @Column(name="numero_pista")
    private int numeroPista;

    @Column(name="fecha_salida")
    private Date fechaSalida;

    @ManyToOne
    @JoinColumn(name="id_artista")
    private Artista artista;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String nombre) {
        this.titulo = nombre;
    }

    public float getPrecioItunes() {
        return precioItunes;
    }

    public void setPrecioItunes(float precioItunes) {
        this.precioItunes = precioItunes;
    }

    public int getNumeroPista() {
        return numeroPista;
    }

    public void setNumeroPista(int numeroPista) {
        this.numeroPista = numeroPista;
    }

    public Date getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(Date fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public Artista getArtista() {
        return artista;
    }

    public void setArtista(Artista artista) {
        this.artista = artista;
    }

    public String toString(){
        return titulo+" - "+artista+" ("+precioItunes+"€)";
    }
}
