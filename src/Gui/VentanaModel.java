package Gui;

import Base.*;
import Util.Util;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import Util.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

import Gui.*;
import org.hibernate.Session;


/**
 * Created by sergio on 06/02/2016.
 */
public class VentanaModel {

    private ArrayList<ArtistasConciertos> detalles;

    public VentanaModel(){
        detalles = new ArrayList<>();
    }

    public void conectar() {
        try {
            HibernateUtil.buildSessionFactory();
            HibernateUtil.openSession();

        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }

    public void desconectar() {
        try {
            HibernateUtil.closeSessionFactory();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }

    public void guardarArtista(Artista artista) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.save(artista);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void modificarArtista(Artista artista) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.update(artista);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void eliminarArtista(Artista artista) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(artista);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public Artista getArtista(String nombre) {

        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Artista a WHERE a.nombre = :nombre");
        query.setParameter("nombre", nombre);
        Artista artista = (Artista) query.uniqueResult();

        return artista;
    }

    public ArrayList<Artista> getArtistas() {

        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Artista");
        ArrayList<Artista> artistas = (ArrayList<Artista>) query.list();

        return artistas;
    }

    public void guardarCancion(Cancion cancion) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.save(cancion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void modificarCancion(Cancion cancion) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.update(cancion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void eliminarCancion(Cancion cancion) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(cancion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public Cancion getCancion(String titulo) {

        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Cancion c WHERE c.titulo = :titulo");
        query.setParameter("titulo", titulo);
        Cancion cancion = (Cancion) query.uniqueResult();

        return cancion;
    }

    public ArrayList<Cancion> getCanciones() {

        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Cancion");
        ArrayList<Cancion> canciones = (ArrayList<Cancion>) query.list();

        return canciones;
    }

    public void guardarDiscografica(Discografica discografica) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.save(discografica);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void modificarDiscografica(Discografica discografica) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.update(discografica);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void eliminarDiscografica(Discografica discografica) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(discografica);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public Discografica getDiscografica(String nombre) {

        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Discografica d WHERE d.nombre = :nombre");
        query.setParameter("nombre", nombre);
        Discografica discografica = (Discografica) query.uniqueResult();

        return discografica;
    }

    public ArrayList<Discografica> getDiscograficas() {

        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Discografica ");
        ArrayList<Discografica> discograficas = (ArrayList<Discografica>) query.list();

        return discograficas;
    }

    public void guardarDisco(Disco disco) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.save(disco);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void modificarDisco(Disco disco) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.update(disco);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void eliminarDisco(Disco disco) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(disco);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public Disco getDisco(String titulo) {

        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Disco d WHERE d.titulo = :titulo");
        query.setParameter("titulo", titulo);
        Disco disco = (Disco) query.uniqueResult();

        return disco;
    }

    public ArrayList<Disco> getDiscos() {

        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Disco ");
        ArrayList<Disco> discos = (ArrayList<Disco>) query.list();

        return discos;
    }

    public void guardarConcierto(Concierto concierto) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.save(concierto);
        for (ArtistasConciertos detalle: detalles){
            detalle.setConcierto(concierto);
            concierto.getArtistaConcierto().add(detalle);
            sesion.save(detalle);
        }
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void modificarConcierto(Concierto concierto) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        concierto.getArtistaConcierto().clear();
        for (ArtistasConciertos detalle: detalles){
            sesion.save(detalle);
        }
        sesion.update(concierto);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void eliminarConcierto(Concierto concierto) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        for (ArtistasConciertos detalle: detalles){
            sesion.delete(detalle);
        }
        sesion.delete(concierto);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public Concierto getConcierto(String nombre) {

        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Concierto c WHERE c.nombre = :nombre");
        query.setParameter("nombre", nombre);
        Concierto concierto = (Concierto) query.uniqueResult();

        return concierto;
    }

    public ArrayList<Concierto> getConciertos() {

        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Concierto ");
        ArrayList<Concierto> conciertos = (ArrayList<Concierto>) query.list();

        return conciertos;
    }

    public ArrayList<ArtistasConciertos> getDetalles(){
        return detalles;
    }

    public void nuevoDetalle(ArtistasConciertos detalle) {
        detalles.add(detalle);
    }

    public ArrayList<ArtistasConciertos> getDetalles(Concierto concierto) {

        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM ArtistasConciertos ac " +
                        "WHERE ac.concierto = :concierto");
        query.setParameter("concierto", concierto);

        ArrayList<ArtistasConciertos> detalles =
                (ArrayList<ArtistasConciertos>) query.list();

        return detalles;
    }

    public void guardarCategoria(Categoria categoria, List<Disco> discos) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        for (Disco disco : discos) {
            categoria.getDiscos().add(disco);
        }
        sesion.save(categoria);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void modificarCategoria(Categoria categoria, List<Disco> discos) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        categoria.getDiscos().clear();
        for (Disco disco : discos) {
            categoria.getDiscos().add(disco);
        }
        sesion.update(categoria);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void eliminarCategoria(Categoria categoria) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(categoria);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public Categoria getCategoria(String nombre) {

        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Categoria c WHERE c.nombre = :nombre");
        query.setParameter("nombre", nombre);
        Categoria categoria = (Categoria) query.uniqueResult();

        return categoria;
    }

    public ArrayList<Categoria> getCategorias() {

        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Categoria ");
        ArrayList<Categoria> categorias = (ArrayList<Categoria>) query.list();

        return categorias;
    }

    public Categoria getDiscosPorCategoria(Categoria recibido) {

        String nombre;
        nombre = recibido.toString();
        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Categoria c WHERE c.nombre = :nombre");
        query.setParameter("nombre", nombre);
        Categoria categoria = (Categoria) query.uniqueResult();

        return categoria;
    }

    public void borrarDetalles(Concierto concierto){
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        for (ArtistasConciertos artistasConciertos: concierto.getArtistaConcierto()){
            sesion.delete(artistasConciertos);
        }
        sesion.getTransaction().commit();
        sesion.close();
    }

    public String loginConectar(String nombre, String contrasena){
        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Usuario u WHERE u.nombre = :nombre and u.contrasena = :contrasena");
        query.setParameter("nombre", nombre);
        query.setParameter("contrasena", contrasena);

        Usuario resultado = (Usuario) query.uniqueResult();

        String rol;
        if (resultado==null){
            rol = "NoRol";
        } else {
            rol = resultado.getRol();
        }

        return rol;
    }


}
