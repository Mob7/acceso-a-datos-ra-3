package Gui;

import Base.*;
import Util.Util;

import javax.swing.*;
import java.awt.event.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created by sergio on 06/02/2016.
 */
public class VentanaController {

    private VentanaModel model;
    private Ventana view;
    private boolean nuevoArtista;
    private boolean nuevaCancion;
    private boolean nuevaDiscografica;
    private boolean nuevoDisco;
    private boolean nuevoConcierto;
    private boolean nuevaCategoria;
    private boolean borrarDetalles;
    private boolean conectado;

    Usuario logeo;


    private enum Tipo {
        Artistas, Canciones, Discograficas, Discos, Conciertos, Categorias
    }

    public VentanaController(VentanaModel model, Ventana view) {
        this.model = model;
        this.view = view;

        addListeners();
        inicializar();
    }

    private void inicializar(){

        model.conectar();
        conectado=false;
        login();

        listar(Tipo.Artistas);
        listar(Tipo.Canciones);
        listar(Tipo.Discograficas);
        listar(Tipo.Discos);
        listar(Tipo.Conciertos);
        listar(Tipo.Categorias);
        anadirCombos();
    }

    private void addListeners(){

        view.btNuevoArtista.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nuevoArtista=true;
                modoEdicion(Tipo.Artistas, true, nuevoArtista);
            }
        });

        view.btNuevoCancion.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nuevaCancion=true;
                modoEdicion(Tipo.Canciones, true, nuevaCancion);
            }
        });

        view.btNuevoDiscografica.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nuevaDiscografica=true;
                modoEdicion(Tipo.Discograficas, true, nuevaDiscografica);
            }
        });

        view.btNuevoDisco.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nuevoDisco=true;
                modoEdicion(Tipo.Discos, true, nuevoDisco);
            }
        });

        view.btNuevoConcierto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nuevoConcierto=true;
                modoEdicion(Tipo.Conciertos, true, nuevoConcierto);
            }
        });

        view.btNuevoCategoria.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nuevaCategoria=true;
                modoEdicion(Tipo.Categorias, true, nuevaCategoria);
                List<Disco> listaDiscosCategorias = model.getDiscos();
                view.modeloListaDiscosCategorias.removeAllElements();
                for (Disco disco : listaDiscosCategorias) {
                    view.modeloListaDiscosCategorias.addElement(disco);
                }
            }
        });

        view.btGuardarArtista.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                insertarFila(Tipo.Artistas, nuevoArtista);
            }
        });

        view.btGuardarCancion.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                insertarFila(Tipo.Canciones, nuevaCancion);
            }
        });

        view.btGuardarDiscografica.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                insertarFila(Tipo.Discograficas, nuevaDiscografica);
            }
        });

        view.btGuardarDisco.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                insertarFila(Tipo.Discos, nuevoDisco);
            }
        });

        view.btGuardarConcierto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view.btBorrarConcierto.setEnabled(false);
                insertarFila(Tipo.Conciertos, nuevoConcierto);
            }
        });

        view.btGuardarCategoria.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                insertarFila(Tipo.Categorias, nuevaCategoria);
            }
        });

        view.btModificarArtista.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nuevoArtista=false;
                modoEdicion(Tipo.Artistas, true, nuevoArtista);
                view.btGuardarArtista.setEnabled(true);
            }
        });

        view.btModificarCancion.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nuevaCancion=false;
                modoEdicion(Tipo.Canciones, true, nuevaCancion);
                view.btGuardarCancion.setEnabled(true);
            }
        });

        view.btModificarDiscografica.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nuevaDiscografica=false;
                modoEdicion(Tipo.Discograficas, true, nuevaDiscografica);
                view.btGuardarDiscografica.setEnabled(true);
            }
        });

        view.btModificarDisco.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nuevoDisco=false;
                modoEdicion(Tipo.Discos, true, nuevoDisco);
                view.btGuardarDisco.setEnabled(true);
            }
        });

        view.btModificarConcierto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view.btAnadirArtistasConcierto.setEnabled(true);
                view.btBorrarConcierto.setEnabled(true);
                nuevoConcierto=false;
                modoEdicion(Tipo.Conciertos, true, nuevoConcierto);
                view.btGuardarConcierto.setEnabled(true);
            }
        });

        view.btModificarCategoria.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nuevaCategoria=false;
                modoEdicion(Tipo.Categorias, true, nuevaCategoria);
                view.btGuardarCategoria.setEnabled(true);
                List<Disco> listaDiscosCategorias = model.getDiscos();
                view.modeloListaDiscosCategorias.removeAllElements();
                for (Disco disco : listaDiscosCategorias) {
                    view.modeloListaDiscosCategorias.addElement(disco);
                }
            }
        });

        view.btEliminarArtista.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                eliminarFila(Tipo.Artistas);
                modoEdicion(Tipo.Artistas, false, true);
            }
        });

        view.btEliminarCancion.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                eliminarFila(Tipo.Canciones);
                modoEdicion(Tipo.Canciones, false, true);
            }
        });

        view.btEliminarDiscografica.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                eliminarFila(Tipo.Discograficas);
                modoEdicion(Tipo.Discograficas, false, true);
            }
        });

        view.btEliminarDisco.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                eliminarFila(Tipo.Discos);
                modoEdicion(Tipo.Discos, false, true);
            }
        });

        view.btEliminarConcierto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                eliminarFila(Tipo.Conciertos);
                modoEdicion(Tipo.Conciertos, false, true);
            }
        });

        view.btEliminarCategoria.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                eliminarFila(Tipo.Categorias);
                modoEdicion(Tipo.Categorias, false, true);
            }
        });

        view.btAnadirArtistasConcierto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view.btGuardarConcierto.setEnabled(true);
                SeleccionArtistas seleccion = new SeleccionArtistas();
                seleccion.mostrarDialogo();

                Concierto concierto = (Concierto) view.listConciertos.getSelectedValue();
                Artista artista = seleccion.getArtista();
                float cache = seleccion.getCache();

                ArtistasConciertos participante = new ArtistasConciertos();
                participante.setArtista(artista);
                participante.setConcierto(concierto);
                participante.setCache(cache);

                model.nuevoDetalle(participante);
                listarDetallesConciertos();
            }
        });

        view.btBorrarConcierto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                borrarDetalles=true;
                view.modeloTablaArtistasXConciertos.setNumRows(0);
            }
        });

        view.listArtista.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                view.btModificarArtista.setEnabled(true);
                view.btEliminarArtista.setEnabled(true);
                mostrarDatos(Tipo.Artistas);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        view.listCancion.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                view.btModificarCancion.setEnabled(true);
                view.btEliminarCancion.setEnabled(true);
                mostrarDatos(Tipo.Canciones);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        view.listDiscografica.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                view.btModificarDiscografica.setEnabled(true);
                view.btEliminarDiscografica.setEnabled(true);
                mostrarDatos(Tipo.Discograficas);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        view.listDisco.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                view.btModificarDisco.setEnabled(true);
                view.btEliminarDisco.setEnabled(true);
                mostrarDatos(Tipo.Discos);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        view.listConciertos.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                view.btModificarConcierto.setEnabled(true);
                view.btEliminarConcierto.setEnabled(true);
                mostrarDatos(Tipo.Conciertos);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        view.listDiscosCategorias.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                view.btGuardarCategoria.setEnabled(true);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        view.listCategorias.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                view.btModificarCategoria.setEnabled(true);
                view.btEliminarCategoria.setEnabled(true);
                mostrarDatos(Tipo.Categorias);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        view.tfBusquedaCancion.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                String texto = view.tfBusquedaCancion.getText();
                if (texto.length() <= 2){
                    listar(Tipo.Canciones);
                } else {
                    mostrarBusquedas(Tipo.Canciones, texto);
                }
            }
        });

        view.tfBusquedaDisco.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                String texto = view.tfBusquedaDisco.getText();
                if (texto.length() <= 2){
                    listar(Tipo.Discos);
                } else {
                    mostrarBusquedas(Tipo.Discos, texto);
                }
            }
        });

    }

    private void modoEdicion(Tipo tipo, boolean editable, boolean nuevo){

        switch (tipo){

            case Artistas:
                view.tfNombreArtista.setEnabled(editable);
                view.tfNombreArtista.setEditable(editable);
                view.tfPrecioArtista.setEnabled(editable);
                view.tfPrecioArtista.setEditable(editable);
                view.tfTelefonoArtista.setEnabled(editable);
                view.tfTelefonoArtista.setEditable(editable);
                view.calFechaArtista.setEnabled(editable);

                if (nuevo){
                    view.tfNombreArtista.setText("");
                    view.tfPrecioArtista.setText("");
                    view.tfTelefonoArtista.setText("");
                    view.calFechaArtista.setDate(null);
                    view.btGuardarArtista.setEnabled(true);
                    view.btModificarArtista.setEnabled(false);
                    view.btEliminarArtista.setEnabled(false);
                    view.lbEstado.setText("Rellena los datos del nuevo artista");
                } else {
                    view.lbEstado.setText("Edite los datos correspondientes");
                }

                break;

            case Canciones:
                view.tfTituloCancion.setEnabled(editable);
                view.tfTituloCancion.setEditable(editable);
                view.tfPrecioCancion.setEnabled(editable);
                view.tfPrecioCancion.setEditable(editable);
                view.tfNumeroPistaCancion.setEnabled(editable);
                view.tfNumeroPistaCancion.setEditable(editable);
                view.calFechaCancion.setEnabled(editable);
                view.cbArtistaCancion.setEnabled(editable);
                view.cbArtistaCancion.setEditable(editable);

                if(nuevo){
                    view.tfTituloCancion.setText("");
                    view.tfPrecioCancion.setText("");
                    view.tfNumeroPistaCancion.setText("");
                    view.calFechaCancion.setDate(null);
                    view.cbArtistaCancion.setSelectedItem(null);
                    view.btGuardarCancion.setEnabled(true);
                    view.btModificarCancion.setEnabled(false);
                    view.btEliminarCancion.setEnabled(false);
                    view.lbEstado.setText("Rellena los datos de la nueva canción");
                } else {
                    view.lbEstado.setText("Edite los datos correspondientes");
                }

                break;

            case Discograficas:
                view.tfNombreDiscografica.setEnabled(editable);
                view.tfNombreDiscografica.setEditable(editable);
                view.tfDineroDiscografica.setEnabled(editable);
                view.tfDineroDiscografica.setEditable(editable);
                view.tfArtistaDiscografica.setEnabled(editable);
                view.tfArtistaDiscografica.setEditable(editable);
                view.calFechaDiscografica.setEnabled(editable);

                if(nuevo){
                    view.tfNombreDiscografica.setText("");
                    view.tfDineroDiscografica.setText("");
                    view.tfArtistaDiscografica.setText("");
                    view.calFechaDiscografica.setDate(null);
                    view.btGuardarDiscografica.setEnabled(true);
                    view.btModificarDiscografica.setEnabled(false);
                    view.btEliminarDiscografica.setEnabled(false);
                    view.lbEstado.setText("Rellena los datos de la nueva discográfica");
                } else {
                    view.lbEstado.setText("Edite los datos correspondientes");
                }

                break;

            case Discos:
                view.tfTituloDisco.setEnabled(editable);
                view.tfTituloDisco.setEditable(editable);
                view.tfPrecioDisco.setEnabled(editable);
                view.tfPrecioDisco.setEditable(editable);
                view.tfCopiasDisco.setEnabled(editable);
                view.tfCopiasDisco.setEditable(editable);
                view.calFechaDisco.setEnabled(editable);
                view.cbArtistaDisco.setEnabled(editable);
                view.cbArtistaDisco.setEditable(editable);
                view.cbDiscograficaDisco.setEnabled(editable);
                view.cbDiscograficaDisco.setEditable(editable);


                if (nuevo){
                    view.tfTituloDisco.setText("");
                    view.tfPrecioDisco.setText("");
                    view.tfCopiasDisco.setText("");
                    view.calFechaDisco.setDate(null);
                    view.cbArtistaDisco.setSelectedItem(null);
                    view.cbDiscograficaDisco.setSelectedItem(null);
                    view.btGuardarDisco.setEnabled(true);
                    view.btModificarDisco.setEnabled(false);
                    view.btEliminarDisco.setEnabled(false);
                    view.lbEstado.setText("Rellena los datos del nuevo disco");
                } else {
                    view.lbEstado.setText("Edite los datos correspondientes");
                }

                break;

            case Conciertos:
                view.tfNombreConcierto.setEnabled(editable);
                view.tfNombreConcierto.setEditable(editable);
                view.tfRecaudacionConcierto.setEnabled(editable);
                view.tfRecaudacionConcierto.setEditable(editable);
                view.tfCiudadConcierto.setEnabled(editable);
                view.tfCiudadConcierto.setEditable(editable);
                view.calFechaConcierto.setEnabled(editable);

                if (nuevo){
                    view.modeloTablaArtistasXConciertos.setNumRows(0);
                    view.tfNombreConcierto.setText("");
                    view.tfRecaudacionConcierto.setText("");
                    view.tfCiudadConcierto.setText("");
                    view.calFechaConcierto.setDate(null);
                    view.btAnadirArtistasConcierto.setEnabled(true);
                    view.btModificarConcierto.setEnabled(false);
                    view.btEliminarConcierto.setEnabled(false);
                    view.lbEstado.setText("Rellena los datos del nuevo concierto");
                } else {
                    view.lbEstado.setText("Edite los datos correspondientes");
                }

                break;

            case Categorias:
                view.tfNombreCategoria.setEnabled(editable);
                view.tfNombreCategoria.setEditable(editable);
                view.tfDescripcionCategoria.setEnabled(editable);
                view.tfDescripcionCategoria.setEditable(editable);

                if(nuevo){
                    view.modeloTablaCategorias.setNumRows(0);
                    view.tfNombreCategoria.setText("");
                    view.tfDescripcionCategoria.setText("");
                    view.btGuardarCategoria.setEnabled(true);
                    view.btModificarCategoria.setEnabled(false);
                    view.btEliminarCategoria.setEnabled(false);
                    view.lbEstado.setText("Rellena los datos de la nueva categoría");
                } else {
                    view.lbEstado.setText("Edite los datos correspondientes");
                }

                break;
        }

    }

    private void insertarFila(Tipo tipo, boolean nuevo){

        switch(tipo){

            case Artistas:
                Artista artista = null;

                if (nuevo){
                    artista = new Artista();
                } else {
                    artista = (Artista) view.listArtista.getSelectedValue();
                }

                artista.setNombre(view.tfNombreArtista.getText());
                artista.setPrecioActuacion(Float.valueOf(view.tfPrecioArtista.getText()));
                artista.setTelefono(Integer.valueOf(view.tfTelefonoArtista.getText()));
                artista.setFechaNacimiento(view.calFechaArtista.getDate());

                if(nuevo){
                    model.guardarArtista(artista);
                    Util.mensajeInformacion("Guardar", "Artista guardado correctamente");
                    view.lbEstado.setText("Artista guardado correctamente");
                } else {
                    if (Util.mensajeConfirmacion("Modificar", "¿Está seguro?") == JOptionPane.NO_OPTION)
                        return;
                    model.modificarArtista(artista);
                    Util.mensajeInformacion("Modificar", "Artista modificado correctamente");
                    view.lbEstado.setText("Artista modificado correctamente");
                }
                modoEdicion(Tipo.Artistas, false, true);
                listar(Tipo.Artistas);
                anadirCombos();

                break;

            case Canciones:
                Cancion cancion = null;

                if (nuevo){
                    cancion = new Cancion();
                } else {
                    cancion = (Cancion) view.listCancion.getSelectedValue();
                }

                Artista artistacancion = (Artista)view.cbArtistaCancion.getSelectedItem();

                cancion.setTitulo(view.tfTituloCancion.getText());
                cancion.setPrecioItunes(Float.valueOf(view.tfPrecioCancion.getText()));
                cancion.setNumeroPista(Integer.valueOf(view.tfNumeroPistaCancion.getText()));
                cancion.setFechaSalida(view.calFechaCancion.getDate());
                cancion.setArtista(artistacancion);

                if(nuevo){
                    model.guardarCancion(cancion);
                    Util.mensajeInformacion("Guardar", "Canción guardada correctamente");
                    view.lbEstado.setText("Canción guardada correctamente");
                } else {
                    if (Util.mensajeConfirmacion("Modificar", "¿Está seguro?") == JOptionPane.NO_OPTION)
                        return;
                    model.modificarCancion(cancion);
                    Util.mensajeInformacion("Modificar", "Canción modificada correctamente");
                    view.lbEstado.setText("Canción modificada correctamente");
                }
                modoEdicion(Tipo.Canciones, false, true);
                listar(Tipo.Canciones);

                break;

            case Discograficas:
                Discografica discografica = null;

                if(nuevo){
                    discografica = new Discografica();
                } else {
                    discografica = (Discografica) view.listDiscografica.getSelectedValue();
                }

                discografica.setNombre(view.tfNombreDiscografica.getText());
                discografica.setDineroFacturado(Float.parseFloat(view.tfDineroDiscografica.getText()));
                discografica.setArtistasContratados(Integer.valueOf(view.tfArtistaDiscografica.getText()));
                discografica.setFechaRegistro(view.calFechaDiscografica.getDate());

                if (nuevo){
                    model.guardarDiscografica(discografica);
                    Util.mensajeInformacion("Guardar", "Discográfica guardada correctamente");
                    view.lbEstado.setText("Discográfica guardada correctamente");
                } else {
                    if (Util.mensajeConfirmacion("Modificar", "¿Está seguro?") == JOptionPane.NO_OPTION)
                        return;
                    model.modificarDiscografica(discografica);
                    Util.mensajeInformacion("Modificar", "Discográfica modificada correctamente");
                    view.lbEstado.setText("Discográfica modificada correctamente");
                }
                modoEdicion(Tipo.Discograficas, false, true);
                listar(Tipo.Discograficas);
                anadirCombos();

                break;

            case Discos:
                Disco disco = null;

                if(nuevo){
                    disco = new Disco();
                } else {
                    disco = (Disco) view.listDisco.getSelectedValue();
                }

                Artista artistadisco = (Artista) view.cbArtistaDisco.getSelectedItem();
                Discografica discodiscografica = (Discografica) view.cbDiscograficaDisco.getSelectedItem();

                disco.setTitulo(view.tfTituloDisco.getText());
                disco.setPrecioCompra(Float.valueOf(view.tfPrecioDisco.getText()));
                disco.setCopiasVendidas(Integer.valueOf(view.tfCopiasDisco.getText()));
                disco.setFechaSalida(view.calFechaDisco.getDate());
                disco.setArtista(artistadisco);
                disco.setDiscografica(discodiscografica);

                if(nuevo){
                    model.guardarDisco(disco);
                    Util.mensajeInformacion("Guardar", "Disco guardado correctamente");
                    view.lbEstado.setText("Disco guardado correctamente");
                } else {
                    if (Util.mensajeConfirmacion("Modificar", "¿Está seguro?") == JOptionPane.NO_OPTION)
                        return;
                    model.modificarDisco(disco);
                    Util.mensajeInformacion("Modificar", "Disco modificado correctamente");
                    view.lbEstado.setText("Disco modificado correctamente");
                }
                modoEdicion(Tipo.Discos, false, true);
                listar(Tipo.Discos);

                break;

            case Conciertos:
                Concierto concierto = null;
                ArtistasConciertos artistaconcierto = null;

                if(nuevo){
                    concierto = new Concierto();
                    artistaconcierto = new ArtistasConciertos();
                } else {
                    concierto = (Concierto) view.listConciertos.getSelectedValue();
                }

                concierto.setNombre(view.tfNombreConcierto.getText());
                concierto.setRecaudacion(Float.valueOf(view.tfRecaudacionConcierto.getText()));
                concierto.setCiudad(view.tfCiudadConcierto.getText());
                concierto.setFecha(view.calFechaConcierto.getDate());

                if(nuevo){
                    model.guardarConcierto(concierto);
                    model.getDetalles().clear();
                    Util.mensajeInformacion("Guardar", "Concierto guardado correctamente");
                    view.lbEstado.setText("Concierto guardado correctamente");
                } else {
                    if (Util.mensajeConfirmacion("Modificar", "¿Está seguro?") == JOptionPane.NO_OPTION)
                        return;
                    if (borrarDetalles=true){
                        model.borrarDetalles(concierto);
                    }
                    model.modificarConcierto(concierto);
                    model.getDetalles().clear();
                    Util.mensajeInformacion("Modificar", "Concierto modificado correctamente");
                    view.lbEstado.setText("Concierto modificado correctamente");
                }
                modoEdicion(Tipo.Conciertos, false, true);
                listar(Tipo.Conciertos);
                view.modeloTablaRepartoConciertos.setNumRows(0);

                break;

            case Categorias:
                Categoria categoria = null;

                if (nuevo){
                    categoria = new Categoria();
                } else {
                    categoria = (Categoria) view.listCategorias.getSelectedValue();
                }

                categoria.setNombre(view.tfNombreCategoria.getText());
                categoria.setDescripcion(view.tfDescripcionCategoria.getText());

                List<Disco> listaDiscos = view.listDiscosCategorias.getSelectedValuesList();

                if (nuevo){
                    model.guardarCategoria(categoria, listaDiscos);
                    Util.mensajeInformacion("Guardar", "Categoría guardada correctamente");
                    view.lbEstado.setText("Categoría guardada correctamente");
                } else {
                    if (Util.mensajeConfirmacion("Modificar", "¿Está seguro?") == JOptionPane.NO_OPTION)
                        return;
                    model.modificarCategoria(categoria, listaDiscos);
                    Util.mensajeInformacion("Modificar", "Categoría modificada correctamente");
                    view.lbEstado.setText("Categoría modificada correctamente");
                }
                modoEdicion(Tipo.Categorias, false, true);
                listar(Tipo.Categorias);
                view.modeloTablaCategorias.setNumRows(0);
                break;
        }

    }

    private void listar(Tipo tipo) {

        switch (tipo){
            case Artistas:
                List<Artista> listaArtistas = model.getArtistas();
                view.modeloListaArtistas.removeAllElements();
                for (Artista artista : listaArtistas) {
                    view.modeloListaArtistas.addElement(artista);
                }
                break;

            case Canciones:
                List<Cancion> listaCanciones= model.getCanciones();
                view.modeloListaCanciones.removeAllElements();
                for (Cancion cancion : listaCanciones) {
                    view.modeloListaCanciones.addElement(cancion);
                }
                break;

            case Discograficas:
                List<Discografica> listaDiscograficas = model.getDiscograficas();
                view.modeloListaDiscograficas.removeAllElements();
                for (Discografica discografica : listaDiscograficas) {
                    view.modeloListaDiscograficas.addElement(discografica);
                }
                break;

            case Discos:
                List<Disco> listaDiscos = model.getDiscos();
                view.modeloListaDiscos.removeAllElements();
                for (Disco disco : listaDiscos) {
                    view.modeloListaDiscos.addElement(disco);
                }
                break;

            case Conciertos:
                List<Concierto> listaConciertos = model.getConciertos();
                view.modeloListaConciertos.removeAllElements();
                for (Concierto concierto : listaConciertos) {
                    view.modeloListaConciertos.addElement(concierto);
                }
                break;

            case Categorias:
                List<Categoria> listaCategorias = model.getCategorias();
                view.modeloListaCategorias.removeAllElements();
                for (Categoria categoria : listaCategorias) {
                    view.modeloListaCategorias.addElement(categoria);
                }
                break;
        }

    }

    private void mostrarDatos(Tipo tipo){

        switch(tipo){

            case Artistas:
                Artista artista = (Artista) view.listArtista.getSelectedValue();

                view.tfNombreArtista.setText(artista.getNombre());
                view.tfPrecioArtista.setText(String.valueOf(artista.getPrecioActuacion()));
                view.tfTelefonoArtista.setText(String.valueOf(artista.getTelefono()));
                view.calFechaArtista.setDate(artista.getFechaNacimiento());

                view.tfNombreArtista.setEnabled(true);
                view.tfNombreArtista.setEditable(false);
                view.tfPrecioArtista.setEnabled(true);
                view.tfPrecioArtista.setEditable(false);
                view.tfTelefonoArtista.setEnabled(true);
                view.tfTelefonoArtista.setEditable(false);
                view.calFechaArtista.setEnabled(true);
                view.lbEstado.setText("Mostrando datos de "+view.tfNombreArtista.getText());
                break;

            case Canciones:
                Cancion cancion = (Cancion) view.listCancion.getSelectedValue();

                view.tfTituloCancion.setText(cancion.getTitulo());
                view.tfPrecioCancion.setText(String.valueOf(cancion.getPrecioItunes()));
                view.tfNumeroPistaCancion.setText(String.valueOf(cancion.getNumeroPista()));
                view.calFechaCancion.setDate(cancion.getFechaSalida());
                view.cbArtistaCancion.getModel().setSelectedItem(cancion.getArtista());

                view.tfTituloCancion.setEnabled(true);
                view.tfTituloCancion.setEditable(false);
                view.tfPrecioCancion.setEnabled(true);
                view.tfPrecioCancion.setEditable(false);
                view.tfNumeroPistaCancion.setEnabled(true);
                view.tfNumeroPistaCancion.setEditable(false);
                view.calFechaCancion.setEnabled(true);
                view.cbArtistaCancion.setEnabled(true);
                view.cbArtistaCancion.setEditable(false);
                view.lbEstado.setText("Mostrando datos de "+"\""+view.tfTituloCancion.getText()+"\"");
                break;

            case Discograficas:
                Discografica discografica = (Discografica) view.listDiscografica.getSelectedValue();

                view.tfNombreDiscografica.setText(discografica.getNombre());
                view.tfDineroDiscografica.setText(String.valueOf(discografica.getDineroFacturado()));
                view.tfArtistaDiscografica.setText(String.valueOf(discografica.getArtistasContratados()));
                view.calFechaDiscografica.setDate(discografica.getFechaRegistro());

                view.tfNombreDiscografica.setEnabled(true);
                view.tfNombreDiscografica.setEditable(false);
                view.tfDineroDiscografica.setEnabled(true);
                view.tfDineroDiscografica.setEditable(false);
                view.tfArtistaDiscografica.setEnabled(true);
                view.tfArtistaDiscografica.setEditable(false);
                view.calFechaDiscografica.setEnabled(true);
                view.lbEstado.setText("Mostrando datos de "+view.tfNombreDiscografica.getText());
                break;

            case Discos:
                Disco disco = (Disco) view.listDisco.getSelectedValue();

                view.tfTituloDisco.setText(disco.getTitulo());
                view.tfPrecioDisco.setText(String.valueOf(disco.getPrecioCompra()));
                view.tfCopiasDisco.setText(String.valueOf(disco.getCopiasVendidas()));
                view.calFechaDisco.setDate(disco.getFechaSalida());
                view.cbArtistaDisco.getModel().setSelectedItem(disco.getArtista());
                view.cbDiscograficaDisco.getModel().setSelectedItem(disco.getDiscografica());

                view.tfTituloDisco.setEnabled(true);
                view.tfTituloDisco.setEditable(false);
                view.tfPrecioDisco.setEnabled(true);
                view.tfPrecioDisco.setEditable(false);
                view.tfCopiasDisco.setEnabled(true);
                view.tfCopiasDisco.setEditable(false);
                view.calFechaDisco.setEnabled(true);
                view.cbArtistaDisco.setEnabled(true);
                view.cbArtistaDisco.setEditable(false);
                view.cbDiscograficaDisco.setEnabled(true);
                view.cbDiscograficaDisco.setEditable(false);
                view.lbEstado.setText("Mostrando datos de "+"\""+view.tfTituloDisco.getText()+"\"");
                break;

            case Conciertos:
                Concierto concierto = (Concierto) view.listConciertos.getSelectedValue();

                view.tfNombreConcierto.setText(concierto.getNombre());
                view.tfRecaudacionConcierto.setText(String.valueOf(concierto.getRecaudacion()));
                view.tfCiudadConcierto.setText(concierto.getCiudad());
                view.calFechaConcierto.setDate(concierto.getFecha());


                view.tfNombreConcierto.setEnabled(true);
                view.tfNombreConcierto.setEditable(false);
                view.tfRecaudacionConcierto.setEnabled(true);
                view.tfRecaudacionConcierto.setEditable(false);
                view.tfCiudadConcierto.setEnabled(true);
                view.tfCiudadConcierto.setEditable(false);
                view.calFechaConcierto.setEnabled(true);

                ArrayList<ArtistasConciertos> detalles= model.getDetalles(concierto);

                view.modeloTablaArtistasXConciertos.setNumRows(0);
                for (ArtistasConciertos conciertos : detalles) {
                    Object[] fila = new Object[]{
                            conciertos.getArtista(),
                            Util.formatMoneda(conciertos.getCache())
                    };
                    view.modeloTablaArtistasXConciertos.addRow(fila);
                }
                view.lbEstado.setText("Mostrando datos de "+view.tfNombreConcierto.getText());
                break;

            case Categorias:
                Categoria categoria = (Categoria) view.listCategorias.getSelectedValue();

                view.tfNombreCategoria.setText(categoria.getNombre());
                view.tfDescripcionCategoria.setText(categoria.getDescripcion());

                view.tfNombreCategoria.setEnabled(true);
                view.tfNombreCategoria.setEditable(false);
                view.tfDescripcionCategoria.setEnabled(true);
                view.tfDescripcionCategoria.setEditable(false);

                view.lbEstado.setText("Mostrando datos de "+view.tfNombreCategoria.getText());

                List<Disco> discoporcategoria = model.getDiscosPorCategoria(categoria).getDiscos();

                view.modeloTablaCategorias.setNumRows(0);
                for (Disco discos : discoporcategoria) {
                    Object[] fila = new Object[]{
                            discos.getTitulo(),
                            discos.getArtista()
                    };
                    view.modeloTablaCategorias.addRow(fila);
                }
                break;
        }

    }

    private void eliminarFila(Tipo tipo){

        switch(tipo){

            case Artistas:
                if (Util.mensajeConfirmacion("Eliminar", "¿Está seguro?") ==
                        JOptionPane.NO_OPTION)
                    return;

                Artista artista = (Artista) view.listArtista.getSelectedValue();
                model.eliminarArtista(artista);
                listar(Tipo.Artistas);
                anadirCombos();
                view.lbEstado.setText("Artista eliminado correctamente");
                break;

            case Canciones:
                if (Util.mensajeConfirmacion("Eliminar", "¿Está seguro?") ==
                        JOptionPane.NO_OPTION)
                    return;

                Cancion cancion = (Cancion) view.listCancion.getSelectedValue();
                model.eliminarCancion(cancion);
                listar(Tipo.Canciones);
                view.lbEstado.setText("Canción eliminada correctamente");
                break;

            case Discograficas:
                if (Util.mensajeConfirmacion("Eliminar", "¿Está seguro?") ==
                        JOptionPane.NO_OPTION)
                    return;

                Discografica discografica = (Discografica) view.listDiscografica.getSelectedValue();
                model.eliminarDiscografica(discografica);
                listar(Tipo.Discograficas);
                anadirCombos();
                view.lbEstado.setText("Discográfica eliminada correctamente");
                break;

            case Discos:
                if (Util.mensajeConfirmacion("Eliminar", "¿Está seguro?") ==
                        JOptionPane.NO_OPTION)
                    return;

                Disco disco = (Disco) view.listDisco.getSelectedValue();
                model.eliminarDisco(disco);
                listar(Tipo.Discos);
                view.lbEstado.setText("Disco eliminado correctamente");
                break;

            case Conciertos:
                if (Util.mensajeConfirmacion("Eliminar", "¿Está seguro?") ==
                        JOptionPane.NO_OPTION)
                    return;

                Concierto concierto = (Concierto) view.listConciertos.getSelectedValue();
                model.eliminarConcierto(concierto);
                listar(Tipo.Conciertos);
                view.lbEstado.setText("Concierto eliminado correctamente");
                break;

            case Categorias:
                if (Util.mensajeConfirmacion("Eliminar", "¿Está seguro?") ==
                        JOptionPane.NO_OPTION)
                    return;

                Categoria categoria = (Categoria) view.listCategorias.getSelectedValue();
                model.eliminarCategoria(categoria);
                listar(Tipo.Categorias);
                view.lbEstado.setText("Categoría eliminada correctamente");
                break;

        }
    }

    private void listarDetallesConciertos(){
        ArrayList<ArtistasConciertos> detalles= model.getDetalles();

        view.modeloTablaRepartoConciertos.setNumRows(0);
        for (ArtistasConciertos conciertos : detalles) {
            Object[] fila = new Object[]{
                    conciertos.getArtista(),
                    Util.formatMoneda(conciertos.getCache())
            };
            view.modeloTablaRepartoConciertos.addRow(fila);
        }
    }

    private void anadirCombos(){
        view.cbArtistaCancion.removeAllItems();
        view.cbArtistaDisco.removeAllItems();
        ArrayList <Artista> artista = model.getArtistas();
        for (int i=0; i<artista.size(); i++) {
            view.cbArtistaCancion.addItem(artista.get(i));
            view.cbArtistaDisco.addItem(artista.get(i));
        }

        view.cbDiscograficaDisco.removeAllItems();
        ArrayList <Discografica> discografica = model.getDiscograficas();
        for (int i=0; i<discografica.size(); i++){
            view.cbDiscograficaDisco.addItem(discografica.get(i));
        }
    }

    private void mostrarBusquedas(Tipo tipo, String busqueda){

        switch(tipo){

            case Canciones:
                List<Cancion> listarBusquedaCanciones = model.getCanciones();
                view.modeloListaCanciones.removeAllElements();
                for (Cancion cancion : listarBusquedaCanciones){
                    if (cancion.getArtista().toString().equalsIgnoreCase(busqueda)){
                        view.modeloListaCanciones.addElement(cancion);
                    }
                    if (cancion.getTitulo().equalsIgnoreCase(busqueda)){
                        view.modeloListaCanciones.addElement(cancion);
                    }
                }

                break;

            case Discos:
                List<Disco> listarBusquedaDiscos = model.getDiscos();
                view.modeloListaDiscos.removeAllElements();
                for(Disco disco : listarBusquedaDiscos){
                    String dineroFacturado = String.valueOf(disco.getDiscografica().getDineroFacturado());
                    String precioActuacion = String.valueOf(disco.getArtista().getPrecioActuacion());
                    if(disco.getTitulo().equalsIgnoreCase(busqueda)){
                        view.modeloListaDiscos.addElement(disco);
                    }
                    if(disco.getArtista().toString().equalsIgnoreCase(busqueda)){
                        view.modeloListaDiscos.addElement(disco);
                    }
                    if(disco.getDiscografica().toString().equalsIgnoreCase(busqueda)){
                        view.modeloListaDiscos.addElement(disco);
                    }
                    if(dineroFacturado.equalsIgnoreCase(busqueda)){
                        view.modeloListaDiscos.addElement(disco);
                    }
                    if(precioActuacion.equalsIgnoreCase(busqueda)){
                        view.modeloListaDiscos.addElement(disco);
                    }
                }

                break;
        }
    }

    private void login() {

        JLogin loginUser = new JLogin("Login");
        loginUser.setVisible(true);

        String usuario = loginUser.getUsuario();
        String contrasena = loginUser.getContrasena();

        String logeo =  model.loginConectar(usuario, contrasena);

        if (logeo.equalsIgnoreCase("Administrador") || logeo.equalsIgnoreCase("Usuario")){
            conectado=true;
            Util.mensajeInformacion("Sesión iniciada correctamente", "Bienvenido "+logeo);
            view.lbEstado.setText("Sesión iniciada correctamente como "+logeo);
        }

        if (conectado==false){
            do {
                Util.mensajeError("Fallo al iniciar sesión", "Los datos introducidos son incorrectos");
                JLogin loginUsuario = new JLogin("Login");
                loginUsuario.setVisible(true);

                usuario  = loginUsuario.getUsuario();
                contrasena = loginUsuario.getContrasena();

                logeo =  model.loginConectar(usuario, contrasena);

                if (logeo.equalsIgnoreCase("Administrador") || logeo.equalsIgnoreCase("Usuario")){
                    conectado=true;
                    Util.mensajeInformacion("Sesión iniciada correctamente", "Bienvenido "+logeo);
                    view.lbEstado.setText("Sesión iniciada correctamente como "+logeo);
                }
            } while (conectado==false);
        }

    }
}
