package Gui;

import Base.Artista;
import Util.HibernateUtil;
import org.hibernate.Query;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.util.List;

/**
 * Created by sergio on 07/02/2016.
 */
public class SeleccionArtistas extends JDialog
        implements ActionListener, KeyListener, FocusListener {
    private JTextField tfBusquedaMostrarArtistas;
    private JTextField tfCacheMostrarArtistas;
    private JTable tableMostrarArtistas;
    private JButton btCancelar;
    private JButton btOk;
    private JPanel contentPane;

    private DefaultTableModel modeloTabla;

    private Artista artista;
    private float cache;

    public SeleccionArtistas(){
        setContentPane(contentPane);
        setLocationRelativeTo(null);
        setModal(true);
        pack();

        inicializar();
    }

    public Artista getArtista() {
        return artista;
    }

    public float getCache() {
        return cache;
    }

    private void inicializar(){

        tfBusquedaMostrarArtistas.requestFocus();
        tfBusquedaMostrarArtistas.addKeyListener(this);
        tfBusquedaMostrarArtistas.addFocusListener(this);

        btCancelar.addActionListener(this);
        btOk.addActionListener(this);

        modeloTabla = new DefaultTableModel();
        tableMostrarArtistas.setModel(modeloTabla);
        modeloTabla.addColumn("Nombre");
        modeloTabla.addColumn("Teléfono");

        listarArtistas();

    }

    public void mostrarDialogo() {
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == btOk) {
            if (tfCacheMostrarArtistas.getText().equals("")) {
                JOptionPane.showMessageDialog(null, "Indica el caché",
                        "Seleccionar Producto", JOptionPane.ERROR_MESSAGE);
                return;
            }

            cache = Integer.parseInt(tfCacheMostrarArtistas.getText());
            artista = getArtistaSeleccionado();
        }
        else {

        }
        setVisible(false);
    }

    private Artista getArtistaSeleccionado() {

        String nombreArtista = (String)
                tableMostrarArtistas.getValueAt(tableMostrarArtistas.getSelectedRow(), 0);

        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Artista a WHERE a.nombre = :nombre");
        query.setParameter("nombre", nombreArtista);
        Artista artista = (Artista) query.uniqueResult();

        return artista;
    }

    private void listarArtistas() {

        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Artista");
        List<Artista> listaArtistas = query.list();
        llenarTabla(listaArtistas);
    }

    private void llenarTabla(List<Artista> listaArtistas) {

        modeloTabla.setNumRows(0);
        for (Artista artista : listaArtistas) {

            Object[] fila = new Object[]{artista.getNombre(),
                   artista.getTelefono()};
            modeloTabla.addRow(fila);
        }
    }

    private void listarArtistas(String nombre) {

        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Artista a WHERE a.nombre = :nombre");
        query.setParameter("nombre", nombre);
        List<Artista> listaArtistas = query.list();
        llenarTabla(listaArtistas);
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

        String texto = tfBusquedaMostrarArtistas.getText();
        if (texto.length() > 2) {
            listarArtistas(texto);
        }
    }

    @Override
    public void focusGained(FocusEvent e) {

        if (e.getSource() == tfBusquedaMostrarArtistas) {
            tfBusquedaMostrarArtistas.selectAll();
        }
    }

    @Override
    public void focusLost(FocusEvent e) {

    }
}
