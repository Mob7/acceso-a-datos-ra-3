package Gui;

import com.toedter.calendar.JDateChooser;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 * Created by sergio on 06/02/2016.
 */
public class Ventana {
    private JPanel panel1;
    public JTabbedPane tabbedPane1;
    public JTextField tfNombreArtista;
    public JTextField tfTelefonoArtista;
    public JTextField tfPrecioArtista;
    public JTextField tfTituloCancion;
    public JTextField tfPrecioCancion;
    public JTextField tfNumeroPistaCancion;
    public JLabel lbEstado;
    public JButton btNuevoCancion;
    public JButton btGuardarCancion;
    public JButton btNuevoArtista;
    public JButton btGuardarArtista;
    public JButton btModificarCancion;
    public JButton btEliminarCancion;
    public JButton btModificarArtista;
    public JButton btEliminarArtista;
    public JComboBox cbArtistaCancion;
    public JComboBox cbArtistaDisco;
    public JComboBox cbDiscograficaDisco;
    public JDateChooser calFechaArtista;
    public JDateChooser calFechaCancion;
    public JTextField tfCopiasVendidas;
    public JTextField tfNombreDiscografica;
    public JTextField tfDineroDiscografica;
    public JTextField tfArtistaDiscografica;
    public JDateChooser calFechaDiscografica;
    public JButton btNuevoDiscografica;
    public JButton btGuardarDiscografica;
    public JButton btModificarDiscografica;
    public JButton btEliminarDiscografica;
    public JTextField tfTituloDisco;
    public JTextField tfPrecioDisco;
    public JTextField tfCopiasDisco;
    public JDateChooser calFechaDisco;
    public JList listArtista;
    public JList listDiscografica;
    public JButton btNuevoDisco;
    public JButton btGuardarDisco;
    public JButton btModificarDisco;
    public JButton btEliminarDisco;
    public JTextField tfBusquedaDisco;
    public JTextField tfBusquedaCancion;
    public JList listCancion;
    public JList listDisco;
    public JTextField tfNombreConcierto;
    public JTextField tfRecaudacionConcierto;
    public JTextField tfCiudadConcierto;
    public JDateChooser calFechaConcierto;
    public JButton btNuevoConcierto;
    public JButton btGuardarConcierto;
    public JButton btModificarConcierto;
    public JButton btEliminarConcierto;
    public JButton btAnadirArtistasConcierto;
    public JList listConciertos;
    public JButton btRepartoConcierto;
    public JTable tableRepartoConcierto;
    public JTable tableArtistasxConciertos;
    public JTextField tfNombreCategoria;
    public JTextField tfDescripcionCategoria;
    public JList listDiscosCategorias;
    public JTable tableCategorias;
    public JList listCategorias;
    public JButton btNuevoCategoria;
    public JButton btGuardarCategoria;
    public JButton btModificarCategoria;
    public JButton btEliminarCategoria;
    public JButton btBorrarConcierto;

    public DefaultListModel modeloListaArtistas;
    public DefaultListModel modeloListaCanciones;
    public DefaultListModel modeloListaDiscograficas;
    public DefaultListModel modeloListaDiscos;
    public DefaultListModel modeloListaConciertos;
    public DefaultTableModel modeloTablaRepartoConciertos;
    public DefaultTableModel modeloTablaArtistasXConciertos;
    public DefaultListModel modeloListaDiscosCategorias;
    public DefaultListModel modeloListaCategorias;
    public DefaultTableModel modeloTablaCategorias;


    public Ventana() {
        JFrame frame = new JFrame("Música");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        inicializar();
    }

    private void inicializar(){
        modeloListaArtistas = new DefaultListModel();
        listArtista.setModel(modeloListaArtistas);

        modeloListaCanciones = new DefaultListModel();
        listCancion.setModel(modeloListaCanciones);

        modeloListaDiscograficas = new DefaultListModel();
        listDiscografica.setModel(modeloListaDiscograficas);

        modeloListaDiscos = new DefaultListModel();
        listDisco.setModel(modeloListaDiscos);

        modeloListaConciertos = new DefaultListModel();
        listConciertos.setModel(modeloListaConciertos);

        modeloListaDiscosCategorias = new DefaultListModel();
        listDiscosCategorias.setModel(modeloListaDiscosCategorias);

        modeloListaCategorias = new DefaultListModel();
        listCategorias.setModel(modeloListaCategorias);

        modeloTablaRepartoConciertos = new DefaultTableModel();
        tableRepartoConcierto.setModel(modeloTablaRepartoConciertos);

        modeloTablaArtistasXConciertos = new DefaultTableModel();
        tableArtistasxConciertos.setModel(modeloTablaArtistasXConciertos);

        modeloTablaCategorias = new DefaultTableModel();
        tableCategorias.setModel(modeloTablaCategorias);

        modeloTablaRepartoConciertos.addColumn("Artista");
        modeloTablaRepartoConciertos.addColumn("Caché");

        modeloTablaArtistasXConciertos.addColumn("Artista");
        modeloTablaArtistasXConciertos.addColumn("Caché");

        modeloTablaCategorias.addColumn("Disco");
        modeloTablaCategorias.addColumn("Autor");

        lbEstado.setText("Programa iniciado correctamente");

    }

}


